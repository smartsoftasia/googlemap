package com.smartsoftasia.library.googlemap.view.widget;

import android.content.Context;
import android.view.MotionEvent;
import android.widget.FrameLayout;

/**
 * Created by androiddev01 on 12/25/2015 AD.
 * TouchableWrapper
 */
public class TouchableWrapper extends FrameLayout {
  public static final String TAG = "TouchableWrapper";

  public interface OnTouchListener {
    void onTouch();

    void onRelease();
  }

  public TouchableWrapper(Context context) {
    super(context);
  }

  private OnTouchListener onTouchListener;

  @Override
  public boolean dispatchTouchEvent(MotionEvent event) {

    switch (event.getAction()) {
      case MotionEvent.ACTION_DOWN:
        onTouchListener.onTouch();
        break;
      case MotionEvent.ACTION_UP:
        onTouchListener.onRelease();
        break;
    }

    return super.dispatchTouchEvent(event);
  }

  public void setTouchListener(OnTouchListener onTouchListener) {
    this.onTouchListener = onTouchListener;
  }
}
