package com.test.application.googlemap;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.FrameLayout;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.smartsoftasia.library.googlemap.view.fragment.TouchableMapFragment;
import com.smartsoftasia.library.googlemap.view.widget.MapStateListener;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
  public static final String TAG = "MainActivity";

  TouchableMapFragment mapFragment;
  FrameLayout frameLayout;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    frameLayout = (FrameLayout) findViewById(R.id.map);

    mapFragment = new TouchableMapFragment();
    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
    transaction.add(R.id.map, mapFragment).commit();

    mapFragment.getMapAsync(this);
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    new MapStateListener(googleMap, mapFragment, this) {

      @Override
      public void onMapTouched() {

      }

      @Override
      public void onMapReleased() {
      }

      @Override
      public void onMapUnsettled() {

      }

      @Override
      public void onMapSettled() {

      }
    };
  }
}
